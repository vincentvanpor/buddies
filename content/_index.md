## Documentation of our project

``[**Site link**](coming soon)``

**ABOUT OUR PROJECT**  
Made from music lovers for music lovers. 
Have you ever dreamed of a site combining the best music resources? 
So, this is it. By combining spotify, youtube and genious,   
we made it possible to search for a song by artist and title to immediately get lyrics, music video and spotify music player.  
It has never been so easy and convenient!

`**Technical Documentation:**`

**Technologies used:**

Build - gradle

Languages - 
Java in backend / JavaScript, TypeScript, HTML, CSS in frontend

FrameWorks - 
Spring Framework and h2 database in backend / Angular in frontend

Backend and frontend connected via proxy

`**Architectural Drawing**`


![Drawing](content/drawing.png)  
![not loaded? see in OneDrive](https://livettu-my.sharepoint.com/:i:/g/personal/arzarv_ttu_ee/ES4N9KHXjPtFlrz4YMpDUdsBmVxBGhVTz6aoYqnAE26GIA?e=GoulVb)
